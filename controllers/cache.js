const Cache = require("../models/cache")
const randomString = require("../utils/random-string");

Cache.create({
    key: "test",
    value: "testValue"
});

module.exports = {
    async send(req,res,next){
        const {item} = req;
        res.json({data: item});
    },

    async getAll(req,res,next){
        try{
        const items = await Cache.find();
        req.item = items;
        next();
        } catch(err) {
            next(err);
        }
    },

    async getOne(req,res,next){
        try{
        const items = await Cache.findOne({
            key: req.params.key
        });
         req.item = items;
         next();
        } catch(err){
            next(err);
        }   
    },

    async getRandomCache(req, res, next){
        try {
            if (req.item) next();
            req.item = await Cache.create({
                key: req.params.key,
                value: randomString()
                
            })
            next();
        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next){
        try {
            const {key, id, ...text} = req.body;
            req.item = await Cache.create({
                key: req.params.key,
                ...text
                
            })
            next();
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next){
        try {
            const {key, id, v, ...text} = req.body;
            req.item = await Cache.findOneAndUpdate({
                key: req.params.key 
            }, {...text});
            next();
        } catch (err) {
            next(err);
        }
    }
}