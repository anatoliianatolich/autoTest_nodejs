const express = require("express");

const router = express.Router();

const cacheController = require("../controllers/cache")

router.get("/",cacheController.getAll, cacheController.send);
router.get("/:key", cacheController.getOne, cacheController.getRandomCache, cacheController.send);
router.post("/:key", cacheController.create, cacheController.send);
router.put("/:key");
router.delete("/");
router.delete("/:key");


module.exports = router;