
const express = require('express');
const app = express();
const bodyParser = require("body-parser");


require("./utils/connect");
const config = require('./config');
const cacheRouter = require('./route/cache');
const RESTError = require('./utils/rest-error');


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true})); // for parsing 

app.use("/cache/", cacheRouter);

app.use((req, res, next)=>{
    throw new RESTError("1", 404, module.filename);
});

app.use((err, req, res, next) => {
    if (config.NODE_ENV === 'test') {
        console.log(err)
    }
    if (config.NODE_ENV === 'development') {
        res.status(err.status || 500).json({
            message: err.message,
            stack: err.stack,
            module: err.moduleName
        });
    } else {
        res.status(err.status || 500).json({
            error: err.message,
        });
    }
})

if(!module.parent){
    app.listen( config.port, ()=>{
        console.log("LISTEN PORT " + config.port);
    });
}

module.exports = app;
