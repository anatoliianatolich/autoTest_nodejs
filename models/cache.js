const mongoose = require('mongoose');

const cacheSchema = new mongoose.Schema({
    key: {
        type: String,
        required: true,
        lowercase: true
    },
    value: {
        type: String,
        required: true,
        trim: true
    },
    TTL: {
        type: Date,
        default: new Date(Date.now()+1e5)
    }

}) 

module.exports = mongoose.model('cache', cacheSchema);