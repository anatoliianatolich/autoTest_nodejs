const config = require("../config");
const mongoose = require('mongoose');

const pass = config.db.pass.length? `:${config.db.pass}` :'';

const auth =config.db.user.length? `${config.db.user}${pass}@`:'';

mongoose.connect(`mongodb://${auth}${config.db.host}:${config.db.port}/${config.db.name}`,
    () => {
        console.log("DB IS STARTED!");
});